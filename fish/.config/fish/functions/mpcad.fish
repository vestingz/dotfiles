function mpcad
	set -l date (date --date="5 days ago" +"%Y%m%d")
	set -l file (mpc search '(modified-since "$date")' | fzf -m -e +s --reverse | cut -f1 -d':' | sed 's/.$//')
	mpc add $file
end
