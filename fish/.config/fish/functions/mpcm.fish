function mpcm
	while true
		set -l list (mpc playlist -f '%position% [(%track%)] %artist% :: %album% :: %title%')
		set -l from (echo $list\n | fzf --reverse | awk '{print $1}')
		set -l to (echo $list\n | fzf --reverse | awk '{print $1}')
		mpc move $from $to
	end
end
