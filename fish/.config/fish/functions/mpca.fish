function mpca
	set -l file (mpc listall -f '%file% : %genre%' | fzf -m -e +s --reverse | cut -f1 -d':' | sed 's/.$//')
	mpc add $file
end
