function mpcd
	set -l id (mpc playlist -f '%position% [(%track%)] %artist% :: %album% :: %title%' | fzf -m --reverse | awk '{print $1}')
	mpc del $id
end
