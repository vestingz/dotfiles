# Defined in - @ line 1
function crf --wraps='cr (fzf -m --reverse)' --description 'alias crf cr (fzf -m --reverse)'
    cr (fzf -m --reverse) $argv;
end
