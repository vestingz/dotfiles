set -x PAGER 'most -w'
set -x EDITOR nano
set -x VISUAL nano
set -x MPD_HOST raspi
set -x DIFFPROG meld

set -x BROWSER /usr/bin/firefox
set -x NNN_FIFO /tmp/nnn.fifo
set -x NNN_TRASH '1'
set -x NNN_ARCHIVE '\\.(7z|a|ace|alz|arc|arj|bz|bz2|cab|cpio|deb|gz|jar|lha|lz|lzh|lzma|lzo|rar|rpm|rz|t7z|tar|tbz|tbz2|tgz|tlz|txz|tZ|tzo|war|xpi|xz|Z|zip)$'
set -x -U GOPATH $HOME/.go

set -x BORG_PASSCOMMAND 'pass show TODO
set -x BORG_REPO 'ssh://TODO
# Format: ssh://user@domain/./backup
#set -x PASSWORD_STORE_DIR 'HOMEDIR/.pass-store/HOSTNAME'

set -gx PATH ~/.cargo/bin ~/.local/bin $PATH

set -x FZF_DEFAULT_COMMAND 'fd --type f'

alias nnn "nnn -d -T t"
alias ls "exa -l --group-directories-first -g"
#alias paru "paru -Syu --upgrademenu --devel"
alias rmm "rm -i (exa -1 | fzf --multi --ansi)"

if test -n "$DESKTOP_SESSION"
    set (gnome-keyring-daemon --start | string split "=")
end

zoxide init fish | source
