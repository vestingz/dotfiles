#!/bin/bash

#requires brotab: https://github.com/balta2ar/brotab

DEFAULT_WIDTH=90

if [ "$@" ]; then
    echo "$@" | cut -d$'\t' -f1 | xargs -L1 bt activate
else
    active_window=`bt active | \grep firefox | awk '{print $1}'`
	selection=`bt list`
	echo "$selection"
    selected=`$1 \
        | head -1 \
        | cut -d$'\t' -f1`
    if [ "$selected" ]; then
        echo "$selected" | xargs -L1 bt activate
    fi
fi
