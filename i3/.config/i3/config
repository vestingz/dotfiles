# i3wm config file

# Set modifier key
set $mod Mod4

# Font for window titles. Will also be used by the bar unless a different font
# is used in the bar {} block below.
#font pango:monospace 8

#font pango:Jetbrains Mono 12
font pango:Hack 12

# Settings

default_border pixel 1
default_floating_border pixel 1
hide_edge_borders smart
workspace 1 output eDP1 DP1
workspace 2 output DP1 eDP1
workspace_auto_back_and_forth yes
no_focus [class="Thunderbird"]
workspace_layout tabbed

# Autostart

exec --no-startup-id /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
# exec --no-startup-id nm-applet
exec --no-startup-id redshift -t 6500:3200 -l 52:13
exec --no-startup-id xbanish
exec --no-startup-id xbacklight -set 100
exec --no-startup-id pactl set-sink-mute 0 1
#exec --no-startup-id picom
exec --no-startup-id i3-autodisplay
#exec --no-startup-id pasystray
exec --no-startup-id gnome-encfs-manager
exec --no-startup-id nextcloud
exec --no-startup-id pacwall
exec --no-startup-id blueman-applet

## Screen brightness controls
bindsym XF86MonBrightnessUp exec xbacklight -inc 10 # increase screen brightness
bindsym XF86MonBrightnessDown exec xbacklight -dec 10 # decrease screen brightness

# Pulseaudio volume control
bindsym XF86AudioRaiseVolume exec --no-startup-id ~/.config/pactl_volume.sh +1%
bindsym XF86AudioLowerVolume exec --no-startup-id ~/.config/pactl_volume.sh -1%
bindsym XF86AudioMute exec --no-startup-id ~/.config/pactl_volume.sh M

bindsym $mod+XF86AudioRaiseVolume exec --no-startup-id mpc volume +5
bindsym $mod+XF86AudioLowerVolume exec --no-startup-id mpc volume -5
bindsym $mod+XF86AudioMute exec --no-startup-id mpc toggle

# Lock and Suspend

bindsym $mod+x exec "i3lock -c 263238 ; systemctl suspend -i"
#bindsym $mod+x exec "~/.config/i3lockscript.sh ; systemctl suspend -i"
bindsym $mod+c exec ~/.config/i3lockscript.sh

# Rofi window switcher
bindsym $mod+s exec "rofi -modi window,emoji -show window ; ~/.config/mousewarp.sh"
#bindsym $mod+s exec "rofi -modi combi,emoji -combi-modi window,ff-tab:~/.config/rofi_activate_tab.sh -show combi"

# Screenshots
bindsym --release Print exec ~/.config/scrots.sh

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# Start a terminal
bindsym $mod+Return exec alacritty

# Kill focused window
bindsym $mod+Shift+q kill

# Start dmenu
bindsym $mod+d exec dmenu_extended_run
bindsym $mod+Shift+d exec dmenu_run

# Change focus
bindsym $mod+Left focus left; exec --no-startup-id ~/.config/mousewarp.sh
bindsym $mod+Down focus down; exec --no-startup-id ~/.config/mousewarp.sh
bindsym $mod+Up focus up; exec --no-startup-id ~/.config/mousewarp.sh
bindsym $mod+Right focus right; exec --no-startup-id ~/.config/mousewarp.sh

# Move focused window
bindsym $mod+Shift+Left move left 50 px
bindsym $mod+Shift+Down move down 50 px
bindsym $mod+Shift+Up move up 50 px
bindsym $mod+Shift+Right move right 50 px

# Move current workspace to secondary monitor

bindsym $mod+p move workspace to output right
bindsym $mod+o move workspace to output left

# Split in horizontal orientation
bindsym $mod+h split v

# Split in vertical orientation
bindsym $mod+v split h

# Enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# Change container layout (stacked, tabbed, toggle split)
#bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# Toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# Change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# Focus the parent container
bindsym $mod+q focus parent

# Focus the child container
bindsym $mod+a focus child

# Define names for default workspaces for which we configure key bindings later on.
set $ws1 "1"
set $ws2 "2"
set $ws3 "3"
set $ws4 "4"
set $ws5 "5"
set $ws6 "6"
set $ws7 "7"
set $ws8 "8"
set $ws9 "9"
set $ws10 "10"

# Switch to workspace
bindsym $mod+1 workspace $ws1; exec --no-startup-id ~/.config/mousewarp.sh
bindsym $mod+2 workspace $ws2; exec --no-startup-id ~/.config/mousewarp.sh
bindsym $mod+3 workspace $ws3; exec --no-startup-id ~/.config/mousewarp.sh
bindsym $mod+4 workspace $ws4; exec --no-startup-id ~/.config/mousewarp.sh
bindsym $mod+5 workspace $ws5; exec --no-startup-id ~/.config/mousewarp.sh
bindsym $mod+6 workspace $ws6; exec --no-startup-id ~/.config/mousewarp.sh
bindsym $mod+7 workspace $ws7; exec --no-startup-id ~/.config/mousewarp.sh
bindsym $mod+8 workspace $ws8; exec --no-startup-id ~/.config/mousewarp.sh
bindsym $mod+9 workspace $ws9; exec --no-startup-id ~/.config/mousewarp.sh
bindsym $mod+0 workspace $ws10; exec --no-startup-id ~/.config/mousewarp.sh

# Move focused container to workspace
bindsym $mod+Shift+1 move container to workspace $ws1; workspace 1
bindsym $mod+Shift+2 move container to workspace $ws2; workspace 2
bindsym $mod+Shift+3 move container to workspace $ws3; workspace 3
bindsym $mod+Shift+4 move container to workspace $ws4; workspace 4
bindsym $mod+Shift+5 move container to workspace $ws5; workspace 5
bindsym $mod+Shift+6 move container to workspace $ws6; workspace 6
bindsym $mod+Shift+7 move container to workspace $ws7; workspace 7
bindsym $mod+Shift+8 move container to workspace $ws8; workspace 8
bindsym $mod+Shift+9 move container to workspace $ws9; workspace 9
bindsym $mod+Shift+0 move container to workspace $ws10; workspace 10

# Reload the configuration file
bindsym $mod+Shift+c reload

# Restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart

# Exit i3 (logs you out of your X session)
bindsym $mod+Shift+e exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -B 'Yes, exit i3' 'i3-msg exit'"

# Resize window (you can also use the mouse for that)
mode "resize" {

        bindsym Left resize shrink width 10 px or 10 ppt
        bindsym Down resize grow height 10 px or 10 ppt
        bindsym Up resize shrink height 10 px or 10 ppt
        bindsym Right resize grow width 10 px or 10 ppt

        # back to normal: Enter or Escape or $mod+r
        bindsym Return mode "default"
        bindsym Escape mode "default"
        bindsym $mod+r mode "default"
}

bindsym $mod+r mode "resize"

# Start i3bar to display a workspace bar (plus the system information i3status
# finds out, if available)
# Basic bar configuration using the Base16 variables.
bar {
    output eDP1
    tray_output eDP1
    status_command py3status -b
    position bottom
	colors {
	  background #263238
	  statusline #e7e8e6

	  focused_workspace  $base0D $base0D $base00
	  active_workspace   $base02 $base01 $base03
	  inactive_workspace $base01 $base01 $base03
	  urgent_workspace   $base08 $base08 $base07
	  binding_mode       $base08 $base08 $base07
	}
}

bar {
    output HDMI2
    tray_output none
    colors {
	  background #263238
	  statusline #e7e8e6

	  focused_workspace  $base0D $base0D $base00
	  active_workspace   $base02 $base01 $base03
	  inactive_workspace $base01 $base01 $base03
	  urgent_workspace   $base08 $base08 $base07
	  binding_mode       $base08 $base08 $base07
	}
}

# Define colors
set $base00 #002b36
set $base01 #263238
set $base02 #586e75
set $base03 #657b83
set $base04 #839496
set $base05 #93a1a1
set $base06 #eee8d5
set $base07 #fdf6e3
set $base08 #dc322f
set $base09 #cb4b16
set $base0A #b58900
set $base0B #859900
set $base0C #2aa198
set $base0D #268bd2
set $base0E #6c71c4
set $base0F #d33682

# class        border  bground text    indicator child_border
client.focused	$base0D	$base0D	$base00	#F86666	$base05
client.focused_inactive	$base02	$base02	$base03	$base01
client.unfocused	$base01	$base01	$base03	$base01
client.urgent	$base02	$base08	$base07	$base08
